

#ifndef METODS_H
#define METODS_H

#define X_POS       0
#define Y_POS       1
#define X_VEL       2
#define Y_VEL       3
#define X_ACC       4
#define Y_ACC       5
#define YAW         6
#define YAW_RATE    7

#define E_X_VEL       0
#define E_LAT_POS_INT 1
#define E_LAT_POS     2
#define E_LAT_VEL     3
#define E_YAW         4
#define E_YAW_RATE    5

#define X_REF        0
#define Y_REF        1
#define YAW_REF      2
#define RADIUS_REF   3
#define VEL_REF      4
#define YAW_RATE_REF 5

#define X_TRACK       0
#define Y_TRACK       1
#define YAW_TRACK     2
#define RADIUS_TRACK  3

#define PAR_m       0
#define PAR_a       1
#define PAR_b       2
#define PAR_L       3
#define PAR_mu      4
#define PAR_Iz      5
#define PAR_rfw     6
#define PAR_rrw     7
#define PAR_mrw     8
#define PAR_mfw     9
#define PAR_Iwr     10
#define PAR_Iwf     11
#define PAR_Cyr     12
#define PAR_Cyf     13
#define PAR_Ck      14
#define PAR_Cd      15
#define PAR_p       16
#define PAR_Af      17
#define PAR_Vx_lin  18

#define RAD2DEG 180.0/M_PI
#define DEG2RAD M_PI/180.0



// Some of these imports are probably unnecessary...lord forgive me
#include "/opt/ros/lunar/include/ros/ros.h"
#include "/opt/ros/lunar/include/rospack/rospack.h"
#include "/usr/include/eigen3/Eigen/Dense"
#include <math.h>
#include <fenv.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <rc_msgs/StateStamped.h>
#include <rc_msgs/ControlSignal.h>
#include "/opt/ros/lunar/include/geometry_msgs/Point.h"
#include "/opt/ros/lunar/include/geometry_msgs/Vector3Stamped.h"

// Open cv
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>

struct stateSpace {
  Eigen::MatrixXf init;
  Eigen::MatrixXf A;
  Eigen::MatrixXf B;
  Eigen::MatrixXf C;
  Eigen::MatrixXf D;
};

struct pid {
  float P;
  float I;
  float D;
  float old_error;
  float int_error;
  float KP;
  float KI;
  float KD;
};


float getDist2D(float x1, float x2, float y1, float y2 );

void setdistBetweenTrackPoints(Eigen::MatrixXf * track, float * dist);

void setReference(Eigen::VectorXf * state,  Eigen::MatrixXf * track,
      Eigen::VectorXf * reference, Eigen::Vector4i * index);

void pathPlanning(Eigen::VectorXf * state,
                  Eigen::Vector2f * refCoordinatesCar0,
                  Eigen::VectorXf * reference,
                  Eigen::VectorXf * error,
                  Eigen::Vector4i * index,
                  Eigen::MatrixXf * track,
                  int maxVel,
                  int minVel,
                  int constVel);

void setErrors(Eigen::VectorXf * state,  Eigen::VectorXf * reference, Eigen::VectorXf * error);

void setControlSignal(Eigen::VectorXf * error,
    stateSpace * hinf_SS,
    Eigen::MatrixXf * controller,
    Eigen::VectorXf * internal_states,
    Eigen::VectorXf * reference,
    rc_msgs::ControlSignal * controlSignal);

void velocityPID(Eigen::VectorXf * error,
                 pid * PID,
                 rc_msgs::ControlSignal * controlSignal,
                 float rate);

void steeringPID(Eigen::VectorXf * error,
                 pid * PID,
                 rc_msgs::ControlSignal * controlSignal,
                 float rate);

void setInitialIndex(Eigen::VectorXf * state, Eigen::Vector4i * index, Eigen::MatrixXf * track, Eigen::Vector2f * refCoordinatesCar0);

void updateIndex(Eigen::VectorXf * state, Eigen::Vector4i * index, Eigen::MatrixXf * track, Eigen::Vector2f * refCoordinatesCar0, float * prevDist2point);

Eigen::MatrixXf loadCSV(const std::string & path);

void convertToEigenVectorXf(rc_msgs::StateStamped data, Eigen::VectorXf * state);

#endif
