#include "../include/methods.h"

Eigen::Vector2f result;
int n = 0;
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void setInitialIndex(Eigen::VectorXf * state,
  Eigen::Vector4i * index,
  Eigen::MatrixXf * track,
  Eigen::Vector2f * refCoordinatesCar0)
  {
    /*
    Sets initial track index based on vehicle location.
    */
    float lastDist = 100;
    float dist;
    for (size_t i = 0; i < track->rows(); i++) {
      dist = getDist2D( (*state)(X_POS), (*track)(i,X_REF), (*state)(Y_POS), (*track)(i,Y_REF));
      if (dist < lastDist)
      {
        lastDist = dist;
        (*index)(0) = i;
      }
    }

    for (size_t i = 0; i < 3; i++) {
      (*index)(i+1) = (*index)(i) + 1;
      if ((*index)(i+1) > (*track).rows() - 1)
      {
        (*index)(i+1) = (*index)(i+1) - (*track).rows();
      }
    }



    (*refCoordinatesCar0)(X_REF) = (*track)( (*index)(1), X_REF );
    (*refCoordinatesCar0)(Y_REF) = (*track)( (*index)(1), Y_REF );


  }
  ////////////////////////////////////////////////////////////////////////////
  ////////////////////////////////////////////////////////////////////////////
  void updateIndex(Eigen::VectorXf * state,
    Eigen::Vector4i * index,
    Eigen::MatrixXf * track,
    Eigen::Vector2f * refCoordinatesCar0,
    float * prevDist2point)
    {
      /*
      Updates the index of the waypoints of the track description as well as updating
      the current reference point.
      */

      Eigen::Vector2f carPos;
      carPos << (*state)(X_POS)+ cosf((*state)(YAW))*0.05, (*state)(Y_POS)+ sinf((*state)(YAW))*0.05;
      float distance = 0.2*(*state)(X_VEL);
      if (distance < 0.2) {
        distance = 0.2;
      }

      float dist2point = getDist2D(carPos(X_POS),
      (*track)(((*index)(1)),X_REF),
      carPos(Y_POS),
      (*track)(((*index)(1)),Y_REF));


      Eigen::Vector2f u;
      Eigen::Vector2f v;
      Eigen::Vector2f trackpoint;




      trackpoint << (*track)(((*index)(1)),X_REF),(*track)(((*index)(1)),Y_REF);

      v = trackpoint - (0.85*trackpoint + 0.15*carPos);
      u = v/v.norm();
      (*refCoordinatesCar0) = carPos + u*distance;


      float dist2pointfromRef = getDist2D((*refCoordinatesCar0)(X_REF),
      (*track)(((*index)(1)),X_REF),
      (*refCoordinatesCar0)(Y_REF),
      (*track)(((*index)(1)),Y_REF));

      float changeDist = 0.05;
      // If the reference is close enough to the current waypoint, change to the next one.
      if (dist2pointfromRef < changeDist )
      {
        for (size_t i = 0; i < 4; i++) {
          (*index)(i) = (*index)(i) + 1;
          if ((*index)(i) > (*track).rows() - 1)
          {
            (*index)(i) = (*index)(i) - (*track).rows();
          }
        }
      }
    }

    void setErrors(Eigen::VectorXf * state,  Eigen::VectorXf * reference, Eigen::VectorXf * error)
    {

    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    float getDist2D(float x1, float x2, float y1, float y2 )
    {
      return sqrt( pow( (x1-x2), 2 ) + pow( (y1-y2), 2 ) );
    }
    ////////////////////////////////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////
    void setReference(Eigen::VectorXf * state,  Eigen::MatrixXf * track, Eigen::VectorXf * reference , Eigen::Vector4i * index)
    {

    }

    void pathPlanning(Eigen::VectorXf * state,
      Eigen::Vector2f * refCoordinatesCar0,
      Eigen::VectorXf * reference,
      Eigen::VectorXf * error,
      Eigen::Vector4i * index,
      Eigen::MatrixXf * track,
      int maxVel,
      int minVel,
      int constVel)
      {
        /*
        Calculates the path to next point (yaw and yawrate) and outputs the error.
        */
        float maxVelf = ((float) maxVel)/10.0;
        float minVelf = ((float) minVel)/10.0;
        float constVelf = ((float) constVel)/10.0;


        float carX = (*state)(X_POS);
        float carY = (*state)(Y_POS);

        float refX = (*refCoordinatesCar0)(0);
        float refY = (*refCoordinatesCar0)(1);

        float frontofCarX = carX + cosf((*state)(YAW))*0.01;
        float frontofCarY = carY + sinf((*state)(YAW))*0.01;


        Eigen::Vector3f yawVec(3);
        yawVec(0) = frontofCarX - carX;
        yawVec(1) = frontofCarY - carY;
        yawVec(2) = 0.0; //added for crossproduct calculations


        Eigen::Vector3f yawRefVec(3);
        yawRefVec(0) = refX - carX;
        yawRefVec(1) = refY - carY;
        yawRefVec(2) = 0.0; //added for crossproduct calculations

        Eigen::Vector3f angleSign =  yawVec.cross(yawRefVec);

        float dotprodnorm = yawVec.dot(yawRefVec) / (yawVec.norm()*yawRefVec.norm());
        if (dotprodnorm > 1.0)
        {
          dotprodnorm = 1.0;
        }

        (*error)(E_YAW) = (*error)(E_YAW)*0.9 +  0.1*(-std::copysign(acos(dotprodnorm), angleSign(2)));

        float A = getDist2D(carX,frontofCarX,carY,frontofCarY);
        float B = getDist2D(carX,refX,carY,refY);
        float C = getDist2D(frontofCarX,refX,frontofCarY,refY);

        float area = fabs((0.5)*(carX*(frontofCarY-refY) + frontofCarX*(refY-carY) + refX*(carY-frontofCarY)));

        (*reference)(RADIUS_REF) = std::copysign((A*B*C)/(4.0*area), angleSign(2));
        //reduce max radius to 10 meters
        if (fabs((*reference)(RADIUS_REF)) > 10.0)
        {
          (*reference)(RADIUS_REF) = 10.0;
        }
        (*reference)(YAW_RATE_REF) = (*reference)(YAW_RATE_REF)*0.8 + 0.2*((*state)(X_VEL) / (*reference)(RADIUS_REF));

        // TODO: make some nice velocity reference.
        Eigen::Vector3f trackVec(3);
        trackVec(0) = (*track)(((*index)(1)),X_REF) - (*track)(((*index)(2)),X_REF);
        trackVec(1) = (*track)(((*index)(1)),Y_REF) - (*track)(((*index)(2)),Y_REF);
        trackVec(2) = 0.0; //added for crossproduct calculations

        float futureTurnAngle = acos(trackVec.dot(yawVec) / (trackVec.norm()*yawVec.norm()));
        if ( !std::isnan(futureTurnAngle) ) {
          if (constVel > 0) {
            (*reference)(VEL_REF ) = constVelf;
          }
          else {
            (*reference)(VEL_REF ) = maxVelf*((fabs(futureTurnAngle/M_PI) + minVelf)/(1.0 + minVelf));
          }
        }
        (*error)(E_X_VEL) = ((*reference)(VEL_REF ) - (*state)(X_VEL));

        // velocity error

        (*error)(E_LAT_POS) = 0.0; // set distanse errors to zero, just follow with angles.
        (*error)(E_LAT_POS_INT) = 0.0;
        (*error)(E_LAT_VEL) = 0.0;
        (*error)(E_YAW_RATE) = (*state)(YAW_RATE) - (*reference)(YAW_RATE_REF);
      }
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      void setdistBetweenTrackPoints(Eigen::MatrixXf * track, float * dist)
      {
        float totalDist = 0;
        for (size_t i = 0; i < track->rows()-1; i++) {
          totalDist += getDist2D( (*track)(i,X_REF), (*track)(i+1,X_REF), (*track)(i,Y_REF), (*track)(i+1,Y_REF));
        }
        *dist = totalDist/(track->rows());
      }
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      Eigen::MatrixXf loadCSV(const std::string & path)
      {
        std::ifstream indata(path);
        std::vector <std::vector<float> > values;
        uint32_t rows = 0;
        uint cols = 0;
        bool runOnce = 1;
        while (indata) {
          std::string line;
          if (!getline( indata, line)) break;
          std::istringstream lineStream(line);
          std::vector <float> record;
          while (lineStream)
          {
            std::string s;
            if (!getline( lineStream, s, ',')) break;
            record.push_back( std::stof(s) );
            if (runOnce) cols = record.size();
          }
          values.push_back(record);
          runOnce = 0;
          ++rows;
        }
        Eigen::MatrixXf parsedCSV(rows,cols);
        for (size_t i = 0; i < cols; i++) {
          for (size_t j = 0; j < rows; j++) {
            parsedCSV(j,i) = values.at(j).at(i);
          }
        }
        return parsedCSV;
      }
      ////////////////////////////////////////////////////////////////////////////
      ////////////////////////////////////////////////////////////////////////////
      void setControlSignal(Eigen::VectorXf * error,
        stateSpace * hinf_SS,
        Eigen::MatrixXf * lqr_controller,
        Eigen::VectorXf * internal_states,
        Eigen::VectorXf * reference,
        rc_msgs::ControlSignal * controlSignal)
        {
          Eigen::VectorXf result;
          /*
          Calculates the control signal sent to the controller including
          translating it from radians to 0--1600--3200 millivolt and sanity checks
          it so it does not tries to turn the wheels more than possible.
          */

          if ((*lqr_controller).cols() > 1) {
            Eigen::Vector2f u_lqr;
            u_lqr(0) = (*error)(E_YAW);
            u_lqr(1) = (*error)(E_YAW_RATE);

            result = -(*lqr_controller)*u_lqr;
          }

          else {
            Eigen::VectorXf u((*hinf_SS).B.cols());
            u(0) = (*error)(E_YAW);
            u(1) = (*error)(E_YAW_RATE);
            if ((*hinf_SS).B.cols() > 2) {
              u(2) = 0.0;//(*reference)(YAW_RATE_REF);
            }

            (*internal_states) = (*hinf_SS).A*(*internal_states) + (*hinf_SS).B*u;
            result = (*hinf_SS).C*(*internal_states) + (*hinf_SS).D*u;
          }

          if (result(0) > 25.0*DEG2RAD) {
            result(0) = 25.0*DEG2RAD;
          }
          if (result(0) < -25.0*DEG2RAD) {
            result(0) = -25.0*DEG2RAD;
          }

          result(0) = (result(0) + 25.0*DEG2RAD)*(3200.0/(2.0*25.0*DEG2RAD));

          controlSignal->delta = result(0);
        }
        ////////////////////////////////////////////////////////////////////////////
        void velocityPID(Eigen::VectorXf * error,
          pid * PID,
          rc_msgs::ControlSignal * controlSignal,
          float rate) {
            /*
            Simple PID implementation controlling the velocity of the vehicle.
            */

            float dt = 1.0/rate;
            PID->P  = PID->KP*(*error)(E_X_VEL);
            // if control signal approches saturation or brake/reverse stop
            // the integral update
            if (controlSignal->power < 3000.0 || controlSignal->power > 1600.0 || fabs(PID->int_error) < 0.1) {
              PID->int_error += (*error)(E_X_VEL)*dt;
            }
            PID->I  = PID->KI*PID->int_error;
            PID->D  = PID->KD*((*error)(E_X_VEL) - PID->old_error )*dt;
            PID->old_error = (*error)(E_X_VEL);

            float power = PID->P + PID->I + PID->D + 1600.0;

            if (power > 3200.0) {
              power = 3200.0;
            }
            if (power < 0.0) {
              power = 0.0;
            }

            // Acceleration limiter
            if (power - controlSignal->power > 7.0) {
              power = controlSignal->power + 7.0;
            }

            controlSignal->power = power;
          }

          void steeringPID(Eigen::VectorXf * error,
            pid * PID,
            rc_msgs::ControlSignal * controlSignal,
            float rate) {

              float dt = 1.0/rate;
              PID->P  = PID->KP*(*error)(E_YAW);
              // if control signal approches saturation or brake/reverse stop
              // the integral update
              if (controlSignal->delta < 3200.0 || controlSignal->delta > 0.0) {
                PID->int_error += (*error)(E_YAW)*dt;
              }
              PID->I  = PID->KI*PID->int_error;
              PID->D  = PID->KD*(*error)(E_YAW_RATE);

              float delta = -(PID->P + PID->I + PID->D) + 1600.0;

              if (delta > 3200.0) {
                delta = 3200.0;
              }
              if (delta < 0.0) {
                delta = 0.0;
              }
              controlSignal->delta = delta;
            }


            ////////////////////////////////////////////////////////////////////////////
            void convertToEigenVectorXf(rc_msgs::StateStamped data, Eigen::VectorXf * state)
            {
              (*state)(X_POS) = data.px;
              (*state)(X_VEL) = data.vx;
              (*state)(Y_POS) = data.py;
              (*state)(Y_VEL) = data.vy;
              (*state)(YAW) = data.theta;
              (*state)(YAW_RATE) = data.omega;
            }
