/*
Software License Agreement (BSD License)
Copyright (c) 2018, Fredrik Macintosh
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions
are met:
* Redistributions of source code must retain the above copyright
notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above
copyright notice, this list of conditions and the following
disclaimer in the documentation and/or other materials provided
with the distribution.
* Neither the name of car_controller nor the names of its
contributors may be used to endorse or promote products derived
from this software without specific prior written permission.
THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
POSSIBILITY OF SUCH DAMAGE.
author: Fredrik Macintosh
*/

// PUBLISHED TOPICS:    /controlSignalCar0
//                      /controlSignalCar1
// SUBSCRIBED TOPIC:    /statesCar0
//                      /statesCar1

//NOTE: To compile you might have to manually link some libraries because my development
// environment is broken, heh.

// Note to reader is that I am in no way a programmer and have limited experience in c++.
// If you see something implemented stupidly implemented or incorrectly, you're most likely
// right. I'm more of a make it work kind of guy. / Fredrik

#include "../include/methods.h"

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// VARIABLES
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

ros::Publisher controlSignalCar0_pub;
ros::Publisher refPointCar0_pub;
ros::Publisher index_pub;
ros::Publisher controlSignalCar1_pub;

rc_msgs::ControlSignal controlSignal;
geometry_msgs::Point refPoint0;
geometry_msgs::Vector3Stamped indexout;

Eigen::MatrixXf track;
Eigen::VectorXf state(8);
Eigen::MatrixXf lqr_controller0;
Eigen::VectorXf reference(6);
Eigen::MatrixXf carParameters;
Eigen::VectorXf errorCar0(6);
Eigen::VectorXf errorCar1(6);

stateSpace ssCar0;
Eigen::VectorXf internal_states;

pid pidVel;
pid pidSteering;

// Define the frequancey to run the controller
// Depending on the controller implementation, the baudrate might be too low for
// higher frequencies
const float rate = 100;
float distBetweenTrackPoints;
float dataSize;
Eigen::Vector4i indexCar0;
Eigen::Vector2f refCoordinatesCar0;
float  prevDist2pointCar0 = 10;
Eigen::Vector4i indexCar1;
Eigen::Vector2f refCoordinatesCar1;

bool cB0FirstRun = 1;
bool cB1FirstRun = 1;

// startup values for velocity control
// TODO: move to init file
int maxVel = 15;
int minVel = 2;
int constVel = 15;


// if LQR or PID_CTRL is true the controller tries to run it
// TODO: implement as an init file instead.
bool LQR = false;
bool PID_CTRL = false;

//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////
// END OF VARIABLES
//////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////

void car0Cb(const rc_msgs::StateStamped &data)
{

  cv::namedWindow("Velocity", 1);
  if (constVel < 1) {
    cv::createTrackbar( "max velocity" , "Velocity", &maxVel, 50);
    cv::createTrackbar( "min velocity" , "Velocity", &minVel, 50);
  }
  cv::createTrackbar( "Constant velocity" , "Velocity", &constVel, 20);
  cv::waitKey(1);

    convertToEigenVectorXf(data, &state);
    if (cB0FirstRun)
    {
        setInitialIndex(&state, &indexCar0, &track, &refCoordinatesCar0);
        cB0FirstRun = 0;
    }
    if (!cB0FirstRun) {
      updateIndex(&state, &indexCar0, &track, &refCoordinatesCar0, &prevDist2pointCar0);
    }
    //setReference(&state, &track, &reference , &indexCar0);
    //setErrors(&state, &reference, &errorCar0);
    pathPlanning(&state, &refCoordinatesCar0, &reference, &errorCar0, &indexCar0, &track, maxVel, minVel, constVel);
    if (PID_CTRL) {
      steeringPID(&errorCar0,&pidSteering,&controlSignal,rate);
    }
    else {
    setControlSignal(&errorCar0, &ssCar0, &lqr_controller0, &internal_states, &reference, &controlSignal);
    }
    velocityPID(&errorCar0,&pidVel,&controlSignal,rate);

      controlSignalCar0_pub.publish(controlSignal);
      // referencepoint at .x and .y, angular error at .z
      refPoint0.x = refCoordinatesCar0(0);
      refPoint0.y = refCoordinatesCar0(1);
      refPoint0.z = errorCar0(E_YAW);
      refPointCar0_pub.publish(refPoint0);
      indexout.vector.x = indexCar0(0);
      indexout.vector.y = indexCar0(1);
      indexout.vector.z = indexCar0(2);
      index_pub.publish(indexout);

}
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
void car1Cb(const rc_msgs::StateStamped &data)
{
//TODO: implement as class function instead for scalability.
}
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////
int main(int argc, char** argv)
{
    ros::init(argc, argv, "car_controller");
    ros::NodeHandle n;
    ros::Rate loop_rate(rate);
if (LQR) {
  std::cout << "LQR controller loaded" << '\n';
}
if (PID_CTRL){
  std::cout << "PID controller loaded" << '\n';
}

//TODO: move the PID initiation to somewhere else.. .csv file?
    pidVel.KP = 560.0;
    pidVel.KI = 150.0;
    pidVel.KD = 63.0;
    pidVel.old_error = 0.0;
    pidVel.int_error = 0.0;

    pidSteering.KP = 1900.0;
    pidSteering.KI = 500.0;
    pidSteering.KD = 150.0;
    pidSteering.old_error = 0.0;
    pidSteering.int_error = 0.0;

    errorCar0 = Eigen::VectorXf::Zero(6);

    controlSignalCar0_pub = n.advertise<rc_msgs::ControlSignal>("control", 1);
    refPointCar0_pub = n.advertise<geometry_msgs::Point>("refPoint0", 1);
    index_pub = n.advertise<geometry_msgs::Vector3Stamped>("index0", 1);
    controlSignalCar1_pub = n.advertise<rc_msgs::ControlSignal>("controlSignalCar1", 1);

    ros::Subscriber stateCar0_sub = n.subscribe("/estState0", 1, car0Cb);
    ros::Subscriber stateCar1_sub = n.subscribe("/estState1", 1, car1Cb);

    //TODO Fix the imports, they reeeeeaaaally shouldn't point to your specific user....
    track  = loadCSV("/home/jlund/CONTROLLER/track.csv");
    //ros::ROS_INFO_STREAM("track loaded");
    if (LQR) {
      lqr_controller0 = loadCSV("/home/jlund/CONTROLLER/lqr.csv");
    }
    else {
      lqr_controller0 = Eigen::MatrixXf::Zero(1,1);
    }

    // load the state space matrices of the discrete hinf controller
    ssCar0.init = loadCSV("/home/jlund/CONTROLLER/init.csv");
    /*
    std::cout << "Controller loaded with following parameters: "<< '\n';
    std::cout << "             DOF: " << ssCar0.init(0,0) << '\n';

    if (ssCar0.init(0,1) > 0 ) {
      std::cout << "Velocity control: enabled" << '\n';
    }
    else {
      std::cout << "Velocity control: disabled" << '\n';
    }
    */

    ssCar0.A = loadCSV("/home/jlund/CONTROLLER/ahd.csv");
    ssCar0.B = loadCSV("/home/jlund/CONTROLLER/bhd.csv");
    ssCar0.C = loadCSV("/home/jlund/CONTROLLER/chd.csv");
    ssCar0.D = loadCSV("/home/jlund/CONTROLLER/dhd.csv");
    internal_states = Eigen::VectorXf::Zero(ssCar0.A.cols());
    if ((!LQR && !PID_CTRL) && ssCar0.B.cols() > 2) {
      std::cout << "2 DOF H_inf controller loaded" << '\n';
    }
    if ((!LQR && !PID_CTRL) && !(ssCar0.B.cols() > 2)) {
      std::cout << "1 DOF H_inf controller loaded" << '\n';
    }
    carParameters = loadCSV("/home/jlund/CONTROLLER/parameters.csv");
    //ros::ROS_INFO_STREAM("car parameters loaded");
    dataSize = track.size();
    setdistBetweenTrackPoints( &track, &distBetweenTrackPoints);
    std::cout << "init done" << '\n';

    // makes sure we have resonable values when starting
    controlSignal.delta = 1600.0;
    controlSignal.power = 1600.0;

    // Loop ROS at claimed rate;
    while (ros::ok())
    {
        ros::spinOnce();
        loop_rate.sleep();
    }
    return 0;
}
